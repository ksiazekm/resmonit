﻿param(
[String]$filename
)

$skiplines = 2
[int]$chartheight = 500
[int]$chartwidth = 1000
[int]$currentychartpos=10

class Header
{
    [String]$headerTitle
    [int]$row
    [int]$column

    Header([String]$headerTitle,[int]$row,[int]$column)
    {
        $this.headerTitle = $headerTitle
        $this.row = $row
        $this.column = $column
    }

    [String]getHeaderTitle()
    {
        return $this.headerTitle
    }

    [int]getRow()
    {
        return $this.row
    }

    [int]getColumn()
    {
        return $this.column
    }

}

function drawchart{
	param($value1,$value2=$null,$yaxistitle=$null,$y2axistitle=$null,$title)


foreach($worksheet in @($wb.worksheets).WHERE({$_.name -LIKE "Graphs*"}) )
	{
		Write-Host "Generating chart $($title) $($worksheet.name)"
		$chart = $worksheet.Shapes.AddChart().Chart
		$chart.ChartType = 4

		foreach($header in $headers)
		{
			if( (Where-Object -InputObject $header -Property headerTitle -match -value "$($value1)") -AND (Where-Object -InputObject $header -Property headerTitle -notmatch -value "$($value2)") )
			{
				$currentseries1 = $chart.SeriesCollection().add($wsdata.Range($wsdata.Cells($startofworkdata+1,$header.getColumn()), $wsdata.Cells($endofworkdata,$header.getColumn())))
				$currentseries1.name = $header.getHeaderTitle()
				$currentseries1.xvalues= $wsdata.Range($wsdata.Cells($startofworkdata+1,1), $wsdata.Cells($endofworkdata,1)) 

				$currentseries1.AxisGroup = 1
			}
			if($value2 -ne $null -and (Where-Object -InputObject $header -Property headerTitle -match -value "$($value2)"))
			{
				$currentseries2 = $chart.SeriesCollection().add($wsdata.Range($wsdata.Cells($startofworkdata+1,$header.getColumn()), $wsdata.Cells($endofworkdata,$header.getColumn())))
				$currentseries2.name = $header.getHeaderTitle()
				$currentseries2.xvalues= $wsdata.Range($wsdata.Cells($startofworkdata+1,1), $wsdata.Cells(($i+$startofworkdata+1),1)) 

				$currentseries2.AxisGroup = 2
			}
		}
		$chart.HasTitle = $true
		$chart.ChartTitle.Text = "$($title)"
		$chart.ChartArea.Top = $currentychartpos
		if($worksheet.name -LIKE "*Detailed")
		{
			$chartwidth = (3 * $endofworkdata)
			$script:currentychartpos += (10 + $script:chartheight)
		}
		$chart.ChartArea.Height = $chartheight
		$chart.ChartArea.Width = "$chartwidth"
		$chart.ChartArea.Left = 10
		$xaxis = $chart.Axes(1,1)
		$yaxis = $chart.Axes(2,1)

		$xaxis.hastitle = $true
		if($xaxis.hastitle)
		{
			$xaxis.axistitle.text = "Time"
		}
		$yaxis.hastitle = $true 
		if($yaxis.hastitle)
		{
			$yaxis.axistitle.text = "$($yaxistitle)"
		}

		if($value2)
		{
			$yaxis2 = $chart.Axes(2,2)
			$yaxis2.hastitle = $true 
			if($yaxis2.hastitle)
			{
				$yaxis2.axistitle.text = "$($y2axistitle)"
			}
		}

	}
}

Write-Host "Generating chart with results, please wait!"
$xl = New-Object -ComObject "Excel.Application"

$excelAxes = [Microsoft.Office.Interop.Excel.XlAxisType]
$xl.SheetsInNewWorkbook = 3
$wb = $xl.Workbooks.Add()
$xlWindows=2
$temp_wb = $xl.workbooks.Open("$((pwd).path)\$($filename)",$false,$false,6,$false,$false,$false,$xlWindows,"^")
$wb.worksheets("Sheet1").name = "Graphs Summary"
$wb.worksheets("Sheet2").name = "Graphs Detailed"
($temp_wb.sheets.item(1)).move($wb.sheets.item(1))
$wb.worksheets(1).name = "Raw Data"

$wsdata = $wb.worksheets.item("Raw Data")
$endofworkdata= $wsdata.usedrange.rows.count

foreach($worksheet in @($wb.worksheets).WHERE({$_.name -ne "Raw Data"}) )
{
	$cells = $worksheet.Cells
	
	$cells.Item(2,8) = "data"
}

$headers = New-Object System.Collections.Generic.List[Header]
$startofworkdata = 3
$cells = $wsdata.Cells
$j=1

do
{
	$cell = $cells.Item($startofworkdata,$j)
#	Write-Host "The cell is: $($cell.text)"
	$headers.Add( (New-Object Header -ArgumentList "$($cell.text)",$startofworkdata,$j) )
	$j++
}while($cell.text)

drawchart ".*MB]" ".*%]" "[MB]" "[%]" "resources usage" 

$wb.worksheets.item("Graphs Summary").Activate()
$xl.Visible=$true
