param(
[PSCredential]$credentialsmi=$(Get-Credential),
[String]$servermi,
[String]$processes,
[String]$ExcelFileName,
[int]$interval = 2
)

$logFileNo = [int]1

while( !($logFileNameFlag) -AND !($ExcelFileName) )
{

	if($logFileNo -lt 10)
	{
		$logFileNoString = "00$($logFileNo)"
	}
	elseif($logFileNo -gt 9 -AND $logFileNo -lt 100)
	{
		$logFileNoString = "0$($logFileNo)"
	}
	else
	{
		$logFileNoString = "$($logFileNo)"
	}

$logFileNameTemp = "monitorremote01_$($logFileNoString).log"

	if( !( Test-Path -Type Leaf -LiteralPath $logFileNameTemp) )
	{
		$logFileNameFlag = $true
		$ExcelFileName = $logFileNameTemp
		$RawDataFileName = "rawDataFile_monitorremote01_$($logFileNoString).log"
	}
	
	$logFileNo += [int]1
}

Write-Host "Logging into $($ExcelFileName)`n"

$wmiquerynumberoflogicalprocessors = "SELECT numberoflogicalprocessors FROM win32_processor"

$wmiqueryprocessesDA = "SELECT name, workingset, percentprocessortime, threadcount, timestamp_sys100ns FROM win32_perfrawdata_perfproc_process WHERE name LIKE '$($processes)%' OR name LIKE 'w3wp%'"

$wminumberoflogicalprocessorsDA = $(Get-WmiObject -ComputerName $servermi -Credential $credentialsmi -Query "$wmiquerynumberoflogicalprocessors ")

$numberoflogicalprocessorsDA = 0 

foreach ($numberoflogicalprocessors in $wminumberoflogicalprocessorsDA)
{
	$numberoflogicalprocessorsDA += $numberoflogicalprocessors.numberoflogicalprocessors
}


	$processesDA = $(Get-WmiObject -ComputerName $servermi -Credential $credentialsmi -Query "$wmiqueryprocessesDA")

	$processesnamesmi = $processesDA.name | select -Unique
	
	Out-File -append -filePath "$($ExcelFileName)" -inputObject "$(Get-Date)`n"

	$outString += "Time^"

	foreach ($name in $processesnamesmi)
	{
		$outString += "$($name) [MB]^"
		$outString += "$($name) [%]^"
		$outString += "$($name) [threads]^"
	}

	Out-File -append -filePath "$($ExcelFileName)" -inputObject "$outString"

	$ramUsage = @{}
	$cpuUsage = @{}
	$threadsQty = @{}
	
	ForEach ($name in $processesnamesmi)
	{
		$ramUsage[$name] = 0
		$cpuUsage[$name] = 0
		$threadsQty[$name] = 0
	}	

	while ($true)
	{

		$processesDAbefore = $(Get-WmiObject -ComputerName $servermi -Credential $credentialsmi -Query "$wmiqueryprocessesDA")

		Start-Sleep 1

		$processesDAafter = $(Get-WmiObject -ComputerName $servermi -Credential $credentialsmi -Query "$wmiqueryprocessesDA")

		ForEach ($processname in $processesnamesmi)
		{
			$ramUsage[$processname] = 0 
			$cpuUsage[$processname] = 0 
			$threadsQty[$processname] = 0 
		
			$beforeProcess = $processesDAbefore.WHERE({$_.name -eq "$($processname)"})
			$afterProcess = $processesDAafter.WHERE({$_.name -eq "$($processname)"})
	
			$ramUsageValue = $afterProcess.workingset / 1MB 
			if($ramUsageValue  -ge 0)
			{			
				$ramUsage[$processName] += $ramUsageValue
			}
			else
			{
				$ramUsage[$processName] += -1
			}
		
			$cpuUsageValue = ( (($afterProcess.percentprocessortime - $beforeProcess.percentprocessortime) / ($afterProcess.timestamp_sys100ns - $beforeProcess.timestamp_sys100ns)) * 100 / $numberoflogicalprocessorsDA) 
				
			if($cpuUsageValue -ge 0 -AND $cpuUsageValue -le 100)
			{			
				$cpuUsage[$processName] += $cpuUsageValue
			}
			else
			{
				$cpuUsage[$processName] += -1
			}

			$threadsQty[$processName] += ( $afterProcess.ThreadCount ) 
		}	
		
	
	    $outString = "$(Get-Date -Format T)^"

	    foreach ($processName in $processesnamesmi)
	    {
		$outString += "$([math]::round($ramUsage[$processName] , 2))^"
		$outString += "$([math]::round($cpuUsage[$processName] , 2))^"
		
		$outString += "$($threadsQty[$processName])^"
	    }

		Out-File -append -filePath "$($ExcelFileName)" -inputObject "$outString"

	}

